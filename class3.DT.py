from colorama import init, Fore, Back, Style
import random
# Necesitamos crear con python un juego de piedra, papel o tijera que cumpla con:

"""
La condición 1 es que pueda jugar contra la maquina
La condición 2 es que pueda jugar mostrar quien gana
La condición 3 es que pueda repetir juego
La condición 4 es que se usen funciones en su desarrollo
La condición 5 es que haga uno de vectores (array)
"""
print (Fore.RED+"JUGEMOS PIEDRA, PAPEL O TIJERAS \n================================ \n")

#ARRAY
options = ["PIEDRA", "PAPEL", "TIJERAS"]

def user():
    #input user
    option_user = input(Fore.GREEN+"ESCRIBA SU OPCION ENTRE: \n PIEDRA \ PAPEL \ TIJERAS \n".upper())
    #option machine
    option_player = random.choice(options)
    print (option_player)
    if option_user.upper() not in options:
        print(Fore.RED+"OPCIÓN INVALIDA")
        user()
    else:
        comparar(option_player, option_user)

#REGLAS
def comparar(pc,player):
    #empatar
    if pc == player:
        print(Fore.RED+"EMPATE!!")
    elif pc == "PIEDRA" and player == "TIJERAS":
        print (Fore.RED+"PERDISTE, VUELVE A INTENTARLO")
    elif pc == "PAPEL" and player == "PIEDRA":
        print (Fore.RED+"PERDISTE, VUELVE A INTENTARLO")
    elif pc == "TIJERAS" and player == "PAPEL":
        print (Fore.RED+"PERDISTE, VUELVE A INTENTARLO")
    else:
        print (Fore.BLUE+"¡¡GANASTE!!")


def jugar():
    while True:
        user()
        res = input(Fore.WHITE+"QUIERES VOLVER A JUGAR S/N: ".upper())
        if res != "S":
            break
jugar()
